# README #

Additional material to reproduce the drug repurposing described in a 2020 submission.

 * FocusHeuristics - independently distributed as an R package on https://bitbucket.org/ibima/focus/
 * Connectivity Map data - R representation - in separate repository on https://bitbucket.org/ibima/cmapreanalysed2016
 * String10HumanDNW_BIG.RData
   This network is an igraph representation of the STRING database. It was input to parameters of the FocusHeuristics that contributed to the drug scoring.
 * jacc.correlation.R - snippet that contributed to the ranking of scores

For questions please contact Stephan Struckmann <firstname.lastname@expressence.de> or Steffen M&ouml;ller <firstname.loestname@uni-rostock.de>.
